import json
import utilities
import os
import operator


# represents connection in graph between actor and movie
class edge(object):

	# constructor
	def __init__(self, actor_node = "", movie_node = ""):
		self.actor_node = actor_node
		self.movie_node = movie_node


# represents node in graph for actor or movie
class node(object):

	# constructor
	def __init__(self, name = "", age_or_year = None, earning = None, is_actor = None, is_movie = None):
		self.name = name
		self.age_or_year = age_or_year
		self.earning = earning
		self.is_actor = is_actor
		self.is_movie = is_movie
		self.edges = []


# Class to create graph -> edges, nodes
class graph(object):

	# constructor
	def __init__(self):
		self.all_actor_nodes = []
		self.all_movie_nodes = []
		self.all_edges = []
		self.nodes_dict = {}

	# add new node in the graph
	def add_new_node(self, name, age_or_year, earning, is_actor, is_movie):
		new_node = node(name, age_or_year, earning, is_actor, is_movie)
		if new_node.is_actor:
			if new_node not in self.all_actor_nodes:
				self.all_actor_nodes.append(new_node)
		elif new_node.is_movie:
			if new_node not in self.all_movie_nodes:
				self.all_movie_nodes.append(new_node)

		return new_node

	# add edge between two nodes
	def add_new_edge(self, actor_node, movie_node):
		new_edge = edge(actor_node, movie_node)
		if new_edge in self.all_edges:
			return

		self.all_edges.append(new_edge)
		if new_edge not in actor_node.edges:
			actor_node.edges.append(new_edge)
		if new_edge not in movie_node.edges:
			movie_node.edges.append(new_edge)

	# create new graph with actors and movies
	def create_new_graph(self, actor_data, movie_data):
		# self.all_actor_nodes = actor_data
		# self.all_movie_nodes = movie_data
		# self.all_edges = []

		for actor in actor_data:
			a_data = actor_data[actor]
			actor_node = self.add_new_node(a_data["name"], a_data["age"], None, True, False)
			self.nodes_dict[actor_node] = a_data
			# print(self.all_actor_nodes[-1].age_or_year == actor_node.age_or_year)
			actor_filmography = a_data["movies"]
			for film in actor_filmography:
				for movie in movie_data:
					m_data = movie_data[movie]
					if m_data["name"] == film[0]:
						movie_node = self.add_new_node(m_data["name"], m_data["year"], m_data["box_office"], False, True)
						self.nodes_dict[movie_node] = m_data
						# duplication handled in add_new_node above
						new_edge = self.add_new_edge(actor_node, movie_node)

		for movie in movie_data:
			m_data = movie_data[movie]
			movie_node = self.add_new_node(m_data["name"], m_data["year"], m_data["box_office"], False, True)
			self.nodes_dict[movie_node] = m_data
			movie_cast = m_data["actors"]
			for cast in movie_cast:
				for actor in actor_data:
					a_data = actor_data[actor]
					if a_data["name"] == cast[0]:
						actor_node = self.add_new_node(a_data["name"], a_data["age"], None, True, False)
						self.nodes_dict[actor_node] = a_data
						new_edge = self.add_new_edge(actor_node, movie_node)

		print("graph created")


	# Graph Queries -

	# Find how much a movie has grossed
	@staticmethod
	def get_movie_earning(name, data):
		for movie in data:
			m_data = data[movie]
			if m_data["name"] == name:
				return m_data["box_office"]
		return None

	# List which movies an actor has worked in
	@staticmethod
	def get_movies_for_actor(name, data):
		films = []
		for actor in data:
			a_data = data[actor]
			if a_data["name"] == name:
				films = a_data["movies"]
		return films

	# List which actors worked in a movie
	@staticmethod
	def get_actors_in_movie(name, data):
		cast = []
		for movie in data:
			m_data = data[movie]
			if m_data["name"] == name:
				cast = m_data["actors"]
		return cast

	# List the top X actors with the most total grossing value
	@staticmethod
	def get_x_actors_by_earning(x, data):
		return None

	# List the oldest X actors
	@staticmethod
	def get_x_oldest_actors(x, data):
		ages = []
		for actor in data:
			a_data = data[actor]
			ages.append([a_data["name"], a_data["age"]])
		ages.sort(key=lambda a: a[1], reverse=True)
		return ages[0:x]

	# List all the movies for a given year
	@staticmethod
	def get_movies_in_year(year, data):
		movies = []
		for movie in data:
			m_data = data[movie]
			if m_data["year"] == year:
				movies.append(m_data["name"])
		return movies

	# List all the actors for a given year
	@staticmethod
	def get_actors_in_year(year, data):
		actors = []
		for movie in data:
			m_data = data[movie]
			if m_data["year"] == year:
				for a in m_data["actors"]:
					if a not in actors:
						actors.append(a)
		return actors

	# Get hub actors
	def get_hub_actors(self, num):
		names = []
		for actor in self.all_actor_nodes:
			names.append(actor)

		# print(names)

		weights = {}
		for name1 in names:
			connection = 0
			films1 = self.nodes_dict[name1]["movies"]
			# print(films1)
			for name2 in names:
				if str(name1) == (name2):
					continue
				films2 = self.nodes_dict[name2]["movies"]
				for film1 in films1:
					if film1 in films2:
						connection += 1
						break

			weights[name1.name] = connection

		connection_sorted_actors = sorted(weights.items(), key=operator.itemgetter(1), reverse=True)
		return connection_sorted_actors[0: num]
		# return 0




if __name__ == '__main__':
	graph = graph()

	directory_path = os.path.dirname(os.path.abspath(__file__))
	data_path = os.path.join(directory_path, "data.json")
	# print(data_path)
	data = utilities.read_json_from_file(data_path)
	# print(data[0])
	# print(data[1])
	graph.create_new_graph(data[0], data[1])

	# print(graph.get_movie_earning("The Story of Us", data[1]))
	# print(graph.get_movies_for_actor("Bruce Willis", data[0]))
	# print(graph.get_actors_in_movie("The Story of Us", data[1]))
	# print(graph.get_x_oldest_actors(10, data[0]))
	# print(graph.get_movies_in_year(1999, data[1]))
	# print(graph.get_actors_in_year(1999, data[1]))

	# print(graph.get_hub_actors(20))

