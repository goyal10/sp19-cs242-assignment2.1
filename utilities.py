import json


# writes json data in filename
def write_json_in_file(filename, data):
	with open(filename + '.json', 'w') as filePointer:
		json.dump(data, filePointer, sort_keys=True, indent=2)

# reads json data from filename
def read_json_from_file(filename):
	with open(filename, 'r') as fileReader:
		return json.load(fileReader)
