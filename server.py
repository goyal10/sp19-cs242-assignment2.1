from flask import Flask, request, abort
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps
from flask_jsonpify import jsonify
import os
import utilities

app = Flask(__name__)

directory_path = os.path.dirname(os.path.abspath(__file__))
data_path = os.path.join(directory_path, "data.json")
# print(data_path)
data = utilities.read_json_from_file(data_path)

actors_data = data[0]
actors_names = []
for name in actors_data:
	actors_names.append(name)

# print(type(actors[0]))

movies_data = data[1]
movies_names = []
for movie in movies_data:
	movies_names.append(movie)


@app.route("/")
def setup_test():
  return "setup correctly"


# GET

# /actors/attr/attr_value
# search for actors with matching attributes and attribute values
# http://127.0.0.1:5000/actors/name/Bruce Willis
@app.route("/actors/<string:attr>/<string:attr_value>", methods = ["GET"])
def get_actors_by_attribute(attr, attr_value):
	ret = []
	# print(attr)
	# print(attr_value)
	# print(attr in actors_data[actors_names[0]])
	if len(actors_names) == 0:
		abort(400)
	if attr not in actors_data[actors_names[0]]:
		abort(400) 
	for name in actors_names:
		data = actors_data[name]
		data_value = data[attr]
		if str(attr_value) in str(data[attr]) or attr_value == data[attr]:
			ret.append(data)
	return jsonify({"_HTTP_": 200, "results": ret})


# /movies?attr={attr_value}
# search for movies with matching attributes and attribute values
# http://127.0.0.1:5000/movies/year/1998
@app.route("/movies/<string:attr>/<string:attr_value>", methods = ["GET"])
def get_movies_by_attribute(attr, attr_value):
	ret = []
	# print(attr in movies_data[movies_names[0]])
	if len(movies_names) == 0:
		abort(400)
	if attr not in movies_data[movies_names[0]]:
		abort(400)
	for name in movies_names:
		data = movies_data[name]
		data_value = data[attr]
		if str(attr_value) in str(data[attr]) or attr_value == data[attr]:
			ret.append(data)
	return jsonify({"_HTTP_": 200, "results": ret})


# /actors/:{actor_name}
# get actor data for matching name
# http://127.0.0.1:5000/actors/Bruce Willis
@app.route("/actors/<string:attr_value>", methods = ["GET"])
def get_actor_by_name(attr_value):
	ret = []
	filter_attr = "name"
	# print(attr_value)
	# print(attr_value in actors_names)
	for name in actors_names:
		data = actors_data[name]
		data_value = actors_data[name][filter_attr]
		if str(data_value) == str(attr_value):
			ret = data
			break
	return jsonify({"_HTTP_": 200, "results": ret})


# /movies/:{movie_name}
# get movie data for matching name
# http://127.0.0.1:5000/movies/Last Man Standing
@app.route("/movies/<string:attr_value>", methods = ["GET"])
def get_movie_by_name(attr_value):
	ret = []
	filter_attr = "name"
	# print(attr_value)
	# print(attr_value in actors_names)
	for name in movies_names:
		data = movies_data[name]
		data_value = movies_data[name][filter_attr]
		if str(data_value) == str(attr_value):
			ret = data
			break
	return jsonify({"_HTTP_": 200, "results": ret})

# curl -i -X PUT -H "Content-Type: application/json" -d '{"total_gross":500}' http://localhost:5002/actors/Bruce%20Willis

# You should also be able to filter using boolean operators AND and OR, i.e. name=”Bob”|name=”Matt”, name=”Bob”&age=35


# PUT

# /actors
# edit actor attributes using name
# http://127.0.0.1:5000/actors/Bruce Willis?age=60&total_gross=20000
@app.route("/actors/<string:attr_value>", methods = ["PUT"])
def edit_actor_data(attr_value):
	ret = []
	if request.args is None:
		abort(400)
	if attr_value not in actors_names:
		abort(400)
	for attr in request.args:
		if attr not in actors_data[attr_value]:
			abort(400)
		actors_data[attr_value][attr] = request.args[attr]
	return jsonify({"_HTTP_": 200, "results": {attr_value: actors_data[attr_value]}})

# /movies
# edit movie attributes using name
# http://127.0.0.1:5000/movies/Hostage?box_office=10&year=2020
@app.route("/movies/<string:attr_value>", methods = ["PUT"])
def edit_movie_data(attr_value):
	ret = []
	if request.args is None:
		abort(400)
	if attr_value not in movies_names:
		abort(400)
	for attr in request.args:
		if attr not in movies_data[attr_value]:
			abort(400)
		movies_data[attr_value][attr] = request.args[attr]
	return jsonify({"_HTTP_": 200, "results": {attr_value: movies_data[attr_value]}})


# POST


# /actors
# add a new actor
# http://127.0.0.1:5000/actors?name=Billy Joe&age=29&json_class=Actor
# http://127.0.0.1:5000/actors/Billy Joe
@app.route("/actors", methods = ["POST"])
def add_new_actor():
	ret = []
	if request.args is None:
		abort(400)
	name_attr = "name"
	if name_attr not in request.args:
		abort(400)
	name_attr_value = request.args[name_attr]
	if name_attr_value in actors_names:
		abort(400)
	actors_names.append(name_attr_value)
	actors_data[name_attr_value] = request.args
	return jsonify({"_HTTP_": 200, "results": ret})

# /movies
# add a new movie
# http://127.0.0.1:5000/movies?name=The Walk&box_office=29&json_class=Movie
# http://127.0.0.1:5000/movies/name/The Walk
@app.route("/movies", methods = ["POST"])
def add_new_movie():
	ret = []
	if request.args is None:
		abort(400)
	name_attr = "name"
	if name_attr not in request.args:
		abort(400)
	name_attr_value = request.args[name_attr]
	if name_attr_value in movies_names:
		abort(400)
	movies_names.append(name_attr_value)
	movies_data[name_attr_value] = request.args
	return jsonify({"_HTTP_": 200, "results": ret})


# DELETE

# /actors/:{actor_name}
# delete actor data for matching name
# http://127.0.0.1:5000/actors/Charlotte Rampling
@app.route("/actors/<string:attr_value>", methods = ["DELETE"])
def delete_actor(attr_value):
	if attr_value not in actors_names:
		abort(400)
	del actors_data[attr_value]
	actors_names.remove(attr_value)
	return jsonify({"_HTTP_": 200, "results": actors_data})


# /movies/:{movie_name}
# delete movie data for matching name
# http://127.0.0.1:5000/movies/Fire with Fire
@app.route("/movies/<string:attr_value>", methods = ["DELETE"])
def delete_movie(attr_value):
	if attr_value not in movies_names:
		abort(400)
	del movies_data[attr_value]
	movies_names.remove(attr_value)
	return jsonify({"_HTTP_": 200, "results": movies_data})


if __name__ == '__main__':
     app.run(port='5002')